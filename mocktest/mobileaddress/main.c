#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

AccountDB t;

void searchAccount() {
  char name[30];
  printf("Enter account's name: ");
  scanf(" %[^\n]s",name);
  struct TreeNode *tmp = searchBST(t,name);
}

void inputAccount() {
  element_t tmp;
  printf("Enter new account's name: ");
  scanf(" %[^\n]s",tmp.name);
  printf("Enter telephone number: ");
  scanf(" %s",tmp.tel);
  printf("Enter balance: ");
  scanf(" %lf",&tmp.balance);
  int flag = inputsearchBST(t,tmp.name,tmp.tel);
  if (flag == -1)
    return;
  else t = insertBST(t,tmp);
}

void removeAccount() {
  char name[30];
  printf("Enter account's name: ");
  scanf(" %[^\n]s",name);
  int flag = removesearchBST(t,name);
  if (flag == 0)
    printf("Name does not exist!");
  else {
    printf("Account with name \"%s\" has been removed!\n",name);
    t = removeBST(t,name);
  }
}

void transfer(){
  char name1[30],name2[30];
  char tel1[15],tel2[15];
  int n1=0,n2=0;
  printf("Enter sender's name: ");
  scanf(" %[^\n]s",name1);
  struct TreeNode* tmp1 = transfersearchBST(t,name1);
  if (tmp1 == NULL) {
    printf("Name does not exist!");
    return;
  }
  if (tmp1->count > 1) {
    printf("Duplicate name! Enter sender's telephone: ");
    scanf(" %s",tel1);
    int flag = 0;
    for (n1 = 0;n1<tmp1->count;n1++) {
      if (strcmp(tel1,tmp1->data[n1].tel)==0) {
        flag =1;
        break;
      }
    }
    if (flag == 0) {
      printf("Account doesn't exist!\n");
      return;
    }
  }
  printf("Enter receiver's name: ");
  scanf(" %[^\n]s",name2);
  struct TreeNode* tmp2 = transfersearchBST(t,name2);
  if (tmp2 == NULL) {
    printf("Name does not exist!");
    return;
  }
  if (tmp2->count > 1) {
    printf("Duplicate name! Enter receiver's telephone: ");
    scanf(" %s",tel2);
    int flag = 0;
    for (n2 = 0;n2<tmp2->count;n2++) {
      if (strcmp(tel2,tmp2->data[n2].tel)==0) {
        flag =1;
        break;
      }
    }
    if (flag == 0) {
      printf("Account doesn't exist!\n");
      return;
    }
  }
  double amount;
  printf("Please enter the amount you want to transfer: ");
  scanf("%lf",&amount);
  if (amount > tmp1->data[n1].balance) {
    printf("Not enough money!");
    return;
  }
  else {
    tmp1->data[n1].balance -= amount;
    tmp2->data[n2].balance += amount;
  }
}

void writefile(FILE* fout, AccountDB t) {
  if (t == NULL) return;
  if (isLeaf(t)) {
    for (int i = 0;i < t->count; i++){
      fprintf(fout,"%s\n",t->name);
      fprintf(fout,"%s\n%lf\n",t->data[i].tel,t->data[i].balance);
    }
    return;
  }
  writefile(fout,t->left);
  for (int i = 0;i < t->count; i++){
    fprintf(fout,"%s\n",t->name);
    fprintf(fout,"%s\n%lf\n",t->data[i].tel,t->data[i].balance);
  }
  writefile(fout,t->right);
}


void writeData() {
	char filename[20];
	printf("Enter file name :");
	scanf(" %s",filename);
  FILE *fout = fopen(filename,"a+");
  fprintf(fout,"%d\n",nodeCount(t));
  writefile(fout,t);
}


void showMenu() {
  printf("\n__________________MENU___________________\n");
  printf("Choose an option to perform:\n");
  printf("1. Read data \n");
  printf("2. Search for an account\n");
  printf("3. Input account\n");
  printf("4. Print all info\n");
  printf("5. Remove account\n");
  printf("6. Transfer money\n");
  printf("7. Write to file\n");
  printf("0. Exit\n");
}

int main() {
  int flag= 1;
  while (flag!=0) {
    showMenu();
    scanf("%d",&flag);
    switch (flag) {
    case 1: t = readData(t,"address.txt");
      break;
    case 2: searchAccount(); break;
    case 3: inputAccount(); break;
    case 4: printf("%-30s%-15s %-10s\n","Name","Tel","Balance");
      inorderTravel(t);

      int d = nodeCount(t);
      printf("\n%d\n",d);
      break;
    case 5: removeAccount();
      break;
    case 6: transfer() ; break;
    case 7: writeData() ; break;
    case 0: break;
    }
  }
}
