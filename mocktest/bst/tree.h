#ifndef _TREE_H_
#define _TREE_H_

struct Account {
  char name[50];
  char telephone[15];
  double balance;
};

typedef struct Account element_t;

struct TreeNode {
  char name[50];
  int count;
  struct Account accounts[5];
  struct TreeNode *left;
  struct TreeNode *right;
};

typedef struct TreeNode* AccountDB;

AccountDB createNullTree();
AccountDB createLeaf(element_t x);
AccountDB createTree(element_t x, AccountDB l, tree r);

int isNullTree(AccountDB t);
int isLeaf(AccountDB t);

AccountDB left(AccountDB t);
AccountDB right(AccountDB t);
element_t value(AccountDB t);

int sizeOfTree(AccountDB t);

struct TreeNode* minBST(AccountDB t);
struct TreeNode* maxBST(AccountDB t);

AccountDB addToLeftMost(AccountDB t, element_t x);
AccountDB addToRightMost(AccountDB t, element_t x); 

AccountDB insertBST(AccountDB t, element_t x);
// xoa nut goc va tra ve cay BST sau khi da xoa
AccountDB removeBST(AccountDB t,element_t x);
struct TreeNode *searchBST(AccountDB t, element_t x);

int height(AccountDB t);
int leavesCount(AccountDB t);
int innerNodeCount(AccountDB t);
int rightChildCount(AccountDB t);

AccountDB reverseBST(AccountDB t);

void inorderTravel(AccountDB t);
  
#endif
