#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

AccountDB createNullTree() {
  return (tree)NULL;
}

AccountDB createTree(element_t x, AccountDB l, AccountDB r) {
  AccountDB t = (tree)malloc(sizeof(struct TreeNode));
  t->accounts[1] = x;
  t->count = 1;
  t->left = l;
  t->right = r;
  return t;
}

AccountDB createLeaf(element_t x) {
  return createTree(x, createNullTree(), createNullTree());
}

int isNullTree(AccountDB t) {
  return (t == NULL);
}

int isLeaf(AccountDB t) {
  return ((t != NULL) &&
          (t->left == NULL) &&
          (t->right == NULL)); 
}

int sizeOfTree(AccountDB t) {
  if (isNullTree(t))
    return 0;
  else
    return (1 + sizeOfTree(t->left) + sizeOfTree(t->right));
}


AccountDB left(AccountDB t) {
  if (isNullTree(t))
    return NULL;
  else return t->left;
}

AccountDB right(AccountDB t) {
  if (isNullTree(t))
    return NULL;
  else return t->right;
}

char* value(AccountDB t) {
  if (isNullTree(t))
    return 0;
  else return t->name;
}

int height(AccountDB t) {
  if (isNullTree(t))
    return 0;
  int left = height(t->left);
  int right = height(t->right);
  if (left>=right)
    return left+1;
  else return right+1;
}

int leavesCount(AccountDB t) {
  int count =0;
  if (isNullTree(t)) return 0;
  if (isLeaf(t)) return 1;
  int left = leavesCount(t->left);
  int right = leavesCount(t->right);
  return left +right;
}
int innerNodeCount(AccountDB t) {
  if(isNullTree(t)) return 0;
  if (isLeaf(t)) return 0;
  if (!isLeaf(t)) return (innerNodeCount(t->left) + innerNodeCount(t->right) +1);

}
int rightChildCount(AccountDB t) {
  if (t == NULL)
    return 0;
  int count = 0;
  if (t->right != NULL)
    count = 1;
  return (count + rightChildCount(t->left) + rightChildCount(t->right));
}

struct TreeNode *searchBST(AccountDB t, char* x) {
  if (isNullTree(t))
    return NULL;
  
  if (strcmp(value(t),x)==0)
    return t;
  else if (strcmp(value(t),x)<0)
    return searchBST(right(t),x);
  else
    return searchBST(left(t),x);
}

AccountDB insertBST(AccountDB t, element_t x) {
  if (isNullTree(t))
    return createLeaf(x);

  if (strcmp(value(t),x.name)==0) {
    t->accounts[count] = x;
    t->count++;
    return t;
  }
  else if (strcmp(value(t),x.name)>0) {
    t->left = insertBST(t->left, x);
    return t;
  } else {
    t->right = insertBST(t->right, x);
    return t;
  }
}
// xoa nut goc va tra ve cay BST sau khi da xoa
AccountDB removeRootBST(AccountDB t) {
  AccountDB p, tmp;
  
  if (isNullTree(t))
    return NULL;
  // Xoa nut la
  if (isLeaf(t)) {
    free(t);
    return NULL;
  }
  // Xoa nut co 1 con phai
  if (left(t) == NULL) {
    tmp = right(t);
    free(t);
    return tmp;
  }
  // Xoa nut co 1 con trai
  if (right(t) == NULL) {
    tmp = left(t);
    free(t);
    return tmp;
  }
  // Xoa nut co hai con
  p = t;
  tmp = right(t);
  while (left(tmp) != NULL) {
    p = tmp;
    tmp = left(tmp);
  }

  t->data = tmp->data;
  tmp = removeRootBST(tmp);
  if (p == t)
    p->right = tmp;
  else
    p->left = tmp;
  return t;
}

AccountDB removeBST(AccountDB t, element_t x) {
  AccountDB p, tmp;
  
  if (isNullTree(t))
    return NULL;

  if (t->data == x)
    return removeRootBST(t);

  p = NULL;
  tmp = t;
  while ((tmp != NULL) && (tmp->data != x)) 
    if (tmp->data > x) {
      p = tmp;
      tmp = tmp->left;
    } else {
      p = tmp;
      tmp = tmp->right;
    }

  if (tmp != NULL) {
    if (p->left == tmp) {
      p->left = removeRootBST(tmp);
    }
    else {
      p->right = removeRootBST(tmp);
    }
  }
  return t;
}

AccountDB reverseBST(AccountDB t) {
  if (t == NULL) return t;
  if (isLeaf(t)) return t;
  AccountDB tmp;
  tmp = t->right;
  t->right = t->left;
  t->left= tmp;
  //free(tmp); 
  t->left = reverseBST(t->left);
  t->right = reverseBST(t->right);
  return t;
}

void inorderTravel(AccountDB t){
  if (t == NULL) {
    //printf("Nil ");
    return;
  }
  if (isLeaf(t)) {
    printf("%d ",value(t));
    return;
  }
  inorderTravel(t->left);
  printf("%d ",value(t));
  inorderTravel(t->right);
}
