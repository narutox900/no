#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "arrayAdd.h"

element_t* read_data(element_t* a){
    FILE* fin = fopen("data.txt","r+");
    element_t* db;
    char buf[100];
	int i = 0,n = 0;
	while (fgets(buf,100,fin))
	{
		n++;
	}
	rewind(fin);
    db = (element_t*)malloc(n*sizeof(element_t));
    while (fgets(buf,100,fin)) {
        sscanf(buf," %[^\n]s",db[i].name);
        printf("%s\n",db[i].name);
        fgets(buf,100,fin);
        sscanf(buf," %[^\n]s",db[i].num);
        printf("%s\n",db[i].num);
        fgets(buf,100,fin);
        sscanf(buf," %[^\n]s",db[i].mail);
        printf("%s\n",db[i].mail);
        i++;
    }
    // printAll(db,n/3);
    fclose(fin);
    return db;
}

int length() {
    FILE* fin = fopen("data.txt","r+");
    element_t* db;
    char buf[100];
	int i = 0,n = 0;
	while (fgets(buf,100,fin))
	{
		n++;
	}
	rewind(fin);
    fclose(fin);
    return n;
}

void printAll(element_t* db,int n){
    for (int i = 0; i<n ; i++) {
        printf("%s\n",db[i].name);
	printf("%s\n",db[i].num);
	printf("%s\n",db[i].mail);
    }
}

element_t* heapSort(element_t* a,int n) {
    for (int i = n / 2 - 1; i >= 0; i--) 
        heapify(a, n, i); 
  
    // One by one extract an element from heap 
    for (int i=n-1; i>0; i--) 
    { 
        // Move current root to end 
        swap(&a[0], &a[i]); 
  
        // call max heapify on the reduced heap 
        heapify(a, i, 0); 
    } 
    return a;
}

void heapify(element_t* a, int n, int i){
    int largest = i; // Initialize largest as root 
    int l = 2*i + 1; // left = 2*i + 1 
    int r = 2*i + 2; // right = 2*i + 2 
  
    // If left child is larger than root 
    if (l < n && strcmp(a[l].name,a[largest].name)>0) 
        largest = l; 
  
    // If right child is larger than largest so far 
    if (r < n && strcmp(a[r].name,a[largest].name)>0) 
        largest = r; 
  
    // If largest is not root 
    if (largest != i) 
    { 
        swap(&a[i], &a[largest]); 
  
        // Recursively heapify the affected sub-tree 
        heapify(a, n, largest); 
    } 

}

element_t *quickSort(element_t* a,int left, int right){
  if (left < right)
    {
      /* pi is partitioning index, arr[p] is now
         at right place */
      int pi = partition(a, left, right);

      // Separately sort elements before
      // partition and after partition
      quickSort(a, left, pi - 1);
      quickSort(a, pi + 1, right);
    }
  return a;
}

int partition (element_t* arr, int low, int high)
{
  char pivot[20];
  strcpy(pivot,arr[high].name);    // pivot
  int i = (low - 1);  // Index of smaller element
  for (int j = low; j <= high- 1; j++)
    {
      // If current element is smaller than the pivot
      if (strcmp(arr[j].name,pivot)<0)
        {
          i++;    // increment index of smaller element
          swap(&arr[i], &arr[j]);
        }
    }
  swap(&arr[i + 1], &arr[high]);
  return (i + 1);
}

void merge (element_t* arr, int l, int m, int r) 
{ 
  int i, j, k; 
  int n1 = m - l + 1; 
  int n2 = r - m; 
  
  /* create temp arrays */
  element_t L[n1], R[n2]; 
  
  /* Copy data to temp arrays L[] and R[] */
  for (i = 0; i < n1; i++) 
    L[i] = arr[l + i]; 
  for (j = 0; j < n2; j++) 
    R[j] = arr[m + 1 + j]; 
  
  /* Merge the temp arrays back into arr[l..r]*/
  i = 0; // Initial index of first subarray 
  j = 0; // Initial index of second subarray 
  k = l; // Initial index of merged subarray 
  while (i < n1 && j < n2) { 
    if (strcmp(L[i].name,R[j].name)<=0) { 
      arr[k] = L[i]; 
      i++; 
    } 
    else { 
      arr[k] = R[j]; 
      j++; 
    } 
    k++; 
  } 
  
  /* Copy the remaining elements of L[], if there 
     are any */
  while (i < n1) { 
    arr[k] = L[i]; 
    i++; 
    k++; 
  } 
  
  /* Copy the remaining elements of R[], if there 
     are any */
  while (j < n2) { 
    arr[k] = R[j]; 
    j++; 
    k++; 
  } 
} 
  
/* l is for left index and r is right index of the 
   sub-array of arr to be sorted */
element_t* mergeSort(element_t* arr, int l, int r) 
{ 
  if (l < r) { 
    // Same as (l+r)/2, but avoids overflow for 
    // large l and h 
    int m = l + (r - l) / 2; 
  
    // Sort first and second halves 
    mergeSort(arr, l, m); 
    mergeSort(arr, m + 1, r); 
  
    merge(arr, l, m, r); 
  }
  return arr;
} 
 

void swap(element_t* a, element_t* b) {
    element_t tmp = *a;
    *a = *b;
    *b = tmp;
}
