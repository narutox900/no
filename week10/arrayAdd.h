#ifndef _ARRAYADD_H_
#define _ARRAYADD_H_

typedef struct mobileaddress
{
    char name[10];
    char num[10];
    char mail[30];
} element_t;

element_t* read_data(element_t* a);

void printAll(element_t* a,int n);
int length();

element_t* heapSort(element_t* a, int n);
void heapify(element_t* a, int n, int i);
void swap(element_t* a, element_t* b);
element_t* quickSort(element_t* a,int left,int right);
int partition (element_t* arr, int low, int high);
void merge (element_t* arr, int l, int m, int r);
element_t* mergeSort(element_t* arr, int l, int r);
#endif
