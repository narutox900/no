#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

CustomerDB t;

void addTransaction() {
  element_t tmp;
  int flag = 0;
  printf("Please enter Customer ID: ");
  scanf(" %s",tmp.id);
  struct TreeNode* x = searchBST(t,tmp.id);
  if (x != NULL) {
    strcpy(tmp.name,x->data.name);
  }
  else {
    do {
      printf("Please enter customer name: ");
      scanf(" %s",tmp.name);
      for (int i =0; i< sizeof(tmp.name)/sizeof(char);i++) {
        if (tmp.name[i] == '@') {
          printf("Name can't contain @, please enter again!\n");
          flag = 1;
          break;
        }
        if (tmp.name[i] == '\0') {
          flag =0;
          break;
        }
      }
    } while (flag!=0);
  }
  do {
    printf("Please enter the amount of electronic payment: ");
    scanf("%ld",&tmp.elec);
    if (tmp.elec <0) {
      printf("The amount of electronic payment must be >=0!\n");
      flag =1;
    } else flag =0;
  } while (flag !=0 );

  do {
    printf("Please enter the amount of fashion payment: ");
    scanf("%ld",&tmp.fas);
    if (tmp.fas <0) {
      printf("The amount of fashion payment must be >=0!\n");
      flag =1;
    }
    else flag =0;
  } while (flag !=0 );
  tmp.total = tmp.elec + tmp.fas;
  if (tmp.elec > 0)
    tmp.elecnum =1;
  else tmp.elecnum=0;
  if (tmp.fas >0)
    tmp.fasnum =1;
  else tmp.fasnum =0;
  t = insertBST(t,tmp);
  printf("Succesfully Updated!\n");

}

void dataAnalysis() {
  struct TreeNode* tmp = findMax(t);
  printf("%-5s Best Customer\n","");
  printf("%-5s",tmp->data.id);
  printf(" %-30s %-25ld %-20d %-20d \n",tmp->data.name,tmp->data.total,tmp->data.elecnum,tmp->data.fasnum);
  printf("\n%-15s%-20s%-40s\n","","Total Payment","Number of transactions");
  printf("%-15s%-20ld%-40d\n","Electronic",totalPaymentElec(t),totalElec(t));
  printf("%-15s%-20ld%-40d\n","Fashion",totalPaymentFas(t),totalFas(t));
}

void showMenu() {
  printf("\n__________________MENU___________________\n");
  printf("Choose an option to perform:\n");
  printf("1. Read data \n");
  printf("2. Print all info\n");
  printf("3. Add transaction\n");
  printf("4. Data analysis\n");
  printf("0. Exit\n");
}

int main() {
  int flag= 1;
  while (flag!=0) {
    showMenu();
    scanf("%d",&flag);
    switch (flag) {
    case 1: t = readData(t,"giaodich.txt");
      break;
    case 2:
      printf("%-5s","ID");
      printf(" %-30s %-25s %-20s %-20s\n","Name","Accumulated payment","Electronics num","Fashion num");
      inorderTravel(t);
      break; 
    case 3: addTransaction(); break;
    case 4: dataAnalysis(); break; 
    case 0: freeTree(t); t= NULL; break;
    }
  }
}
