#ifndef _TREE_H_
#define _TREE_H_

typedef struct {
  char name[50];
  char tel[15];
  double balance;
} Account;

typedef Account element_t;

struct TreeNode {
  char name[50];
  int count;
  element_t data[5];
  struct TreeNode *left;
  struct TreeNode *right;
};

typedef struct TreeNode* AccountDB;

AccountDB createNullTree();
AccountDB createLeaf(element_t x);
AccountDB createTree(element_t x, AccountDB l, AccountDB r);

int isNullTree(AccountDB t);
int isLeaf(AccountDB t);

AccountDB left(AccountDB t);
AccountDB right(AccountDB t);
element_t value(AccountDB t);

int sizeOfTree(AccountDB t);
struct TreeNode* minBST(AccountDB t);
struct TreeNode* maxBST(AccountDB t);
AccountDB insertBST(AccountDB t, element_t x);
// xoa nut goc va tra ve cay BST sau khi da xoa
AccountDB removeBST(AccountDB t,char* x);
struct TreeNode *searchBST(AccountDB t, char* x);

int height(AccountDB t);
int leavesCount(AccountDB t);
int innerNodeCount(AccountDB t);
int rightChildCount(AccountDB t);
int nodeCount(AccountDB t);

AccountDB reverseBST(AccountDB t);

void inorderTravel(AccountDB t);

AccountDB readData(AccountDB t,char* filename);
int inputsearchBST(AccountDB t,char*  x,char* tel);
int removesearchBST(AccountDB t,char*  x);
struct TreeNode* transfersearchBST(AccountDB t, char* x);
#endif
