#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

AccountDB createNullTree() {
  return (AccountDB)NULL;
}

AccountDB createTree(element_t x, AccountDB l, AccountDB r) {
  AccountDB t = (AccountDB)malloc(sizeof(struct TreeNode));
  strcpy(t->name,x.name);
  t->data[0] = x;
  t->count=1;
  t->left = l;
  t->right = r;
  return t;
}

AccountDB createLeaf(element_t x) {
  return createTree(x, createNullTree(), createNullTree());
}

int isNullTree(AccountDB t) {
  return (t == NULL);
}

int isLeaf(AccountDB t) {
  return ((t != NULL) &&
          (t->left == NULL) &&
          (t->right == NULL)); 
}

int sizeOfTree(AccountDB t) {
  if (isNullTree(t))
    return 0;
  else
    return (1 + sizeOfTree(t->left) + sizeOfTree(t->right));
}

AccountDB left(AccountDB t) {
  if (isNullTree(t))
    return NULL;
  else return t->left;
}

AccountDB right(AccountDB t) {
  if (isNullTree(t))
    return NULL;
  else return t->right;
}

struct TreeNode* minBST(AccountDB t) {
  struct TreeNode *n = t;
  
  if (isNullTree(t)) return NULL;
  while (!isNullTree(left(n)))
    n = left(n);
  return n;
}

struct TreeNode* maxBST(AccountDB t) {
  struct TreeNode *n = t;
  
  if (isNullTree(t)) return NULL;
  while (!isNullTree(right(n)))
    n = right(n);
  return n;
}

int height(AccountDB t) {
  if (isNullTree(t))
    return 0;
  int left = height(t->left);
  int right = height(t->right);
  if (left>=right)
    return left+1;
  else return right+1;
}

int leavesCount(AccountDB t) {
  int count =0;
  if (isNullTree(t)) return 0;
  if (isLeaf(t)) return 1;
  int left = leavesCount(t->left);
  int right = leavesCount(t->right);
  return left +right;
}
int innerNodeCount(AccountDB t) {
  if(isNullTree(t)) return 0;
  if (isLeaf(t)) return 0;
  if (!isLeaf(t)) return (innerNodeCount(t->left) + innerNodeCount(t->right) +1);

}
int rightChildCount(AccountDB t) {
  if (t == NULL)
    return 0;
  int count = 0;
  if (t->right != NULL)
    count = 1;
  return (count + rightChildCount(t->left) + rightChildCount(t->right));
}

int nodeCount(AccountDB t) {
  if (t == NULL)
    return 0;
  return (t->count+nodeCount(t->left)+nodeCount(t->right));
}

struct TreeNode *searchBST(AccountDB t,char*  x) {
  if (isNullTree(t))
    return NULL;
  
  if (strcmp(t->name,x) == 0) {
    char tel[15];
    if (t->count >1) {
    printf("Duplicate name! Please enter phone number: ");
    scanf(" %s",tel);
    int flag =0;
    for (int i =0; i<t->count;i++) {
      if (strcmp(tel,t->data[i].tel) == 0) {
        flag = 1;
        printf("%s\n%s\n%lf\n",t->data[i].name,t->data[i].tel,t->data[i].balance);
        break;
      }
    }
    if (flag == 0) {
      printf("Incorrect telephone number!\n");
      return NULL;
    }
    return t;}
    else  printf("%s\n%s\n%lf\n",t->data[0].name,t->data[0].tel,t->data[0].balance);
  }
  else if (strcmp(t->name,x) < 0)
    return searchBST(right(t),x);
  else
    return searchBST(left(t),x);
}

AccountDB insertBST(AccountDB t, element_t x) {
  if (isNullTree(t))
    return createLeaf(x);

  if (strcmp(t->name,x.name) == 0) {
    t->data[t->count] = x;
    t->count++;
    return t;
  }
  else if (strcmp(t->name,x.name)>0) {
    t->left = insertBST(t->left, x);
    return t;
  } else {
    t->right = insertBST(t->right, x);
    return t;
  }
}
// xoa nut goc va tra ve cay BST sau khi da xoa
AccountDB removeRootBST(AccountDB t) {
  AccountDB p, tmp;
  
  if (isNullTree(t))
    return NULL;
  // Xoa nut la
  if (isLeaf(t)) {
    free(t);
    return NULL;
  }
  // Xoa nut co 1 con phai
  if (left(t) == NULL) {
    tmp = right(t);
    free(t);
    return tmp;
  }
  // Xoa nut co 1 con trai
  if (right(t) == NULL) {
    tmp = left(t);
    free(t);
    return tmp;
  }
  // Xoa nut co hai con
  p = t;
  tmp = right(t);
  while (left(tmp) != NULL) {
    p = tmp;
    tmp = left(tmp);
  }

  strcpy(t->name,tmp->name);
  t->count = tmp->count;
  for (int i = 0; i<t->count;i++) {
    strcpy(t->data[i].name,tmp->data[i].name);
    strcpy(t->data[i].tel,tmp->data[i].tel);
    t->data[i].balance = tmp->data[i].balance;
  }
  strcpy(t->name,tmp->name);
  tmp = removeRootBST(tmp);
  if (p == t)
    p->right = tmp;
  else
    p->left = tmp;
  return t;
}

AccountDB removeBST(AccountDB t, char* x) {
  AccountDB p, tmp;
  
  if (isNullTree(t))
    return NULL;

  if (strcmp(t->name,x)==0)
    return removeRootBST(t);

  p = NULL;
  tmp = t;
  while ((tmp != NULL) && (strcmp(tmp->name,x) !=0)) 
    if (strcmp(tmp->name,x)>0) {
      p = tmp;
      tmp = tmp->left;
    } else {
      p = tmp;
      tmp = tmp->right;
    }
  
  if (tmp != NULL) {
    if (p->left == tmp) {
      p->left = removeRootBST(tmp);
    }
    else {
      p->right = removeRootBST(tmp);
    }
  }
  return t;
}

void inorderTravel(AccountDB t){
  if (t == NULL) {
    //printf("Nil ");
    return;
  }
  if (isLeaf(t)) {
    for (int i = 0;i < t->count; i++){
      printf("%-30s",t->name);
      printf("%-15s %-10f\n",t->data[i].tel,t->data[i].balance);
    }
    return;
  }
  inorderTravel(t->left);
  for (int i = 0;i < t->count; i++) {
    printf("%-30s",t->name);
    printf("%-15s %-10f\n",t->data[i].tel,t->data[i].balance);
  }
  inorderTravel(t->right);
}

AccountDB readData(AccountDB t,char* filename) {
  FILE *fin = fopen(filename,"r+");
  char name[50];
  char tel[15];
  double balance;
  char buf[70];
  int n;
  fgets(buf,70,fin);
  sscanf(buf,"%d",&n);
  for (int i = 0; i < n;i++) {
    element_t tmp;
    fgets(buf,70,fin);
    sscanf(buf," %[^\n]s",tmp.name);
    fgets(buf,70,fin);
    sscanf(buf," %s",tmp.tel);
    fgets(buf,70,fin);
    sscanf(buf," %lf",&tmp.balance);
    printf("%s\n%s\n%lf\n",tmp.name,tmp.tel,tmp.balance);
    t = insertBST(t,tmp);
  }
  return t;
}

int inputsearchBST(AccountDB t,char*  x,char* tel) {
  if (isNullTree(t))
    return 0;
  if (strcmp(t->name,x) == 0) {
    int flag =0;
    for (int i =0; i<t->count;i++) {
      if (strcmp(tel,t->data[i].tel) == 0) {
        flag = 1;
        printf("That account already existed\n");
        break;
      }
    }
    if (flag == 1) {
      return -1;
    }
    return 0;
  }
  else if (strcmp(t->name,x) < 0)
    return inputsearchBST(right(t),x,tel);
  else
    return inputsearchBST(left(t),x,tel);
}

int removesearchBST(AccountDB t,char*  x) {
  if (isNullTree(t))
    return 0;

  if (strcmp(t->name,x) == 0) {
    return 1;
  }
  else if (strcmp(t->name,x) < 0)
    return removesearchBST(right(t),x);
  else
    return removesearchBST(left(t),x);
}

struct TreeNode* transfersearchBST(AccountDB t, char* x) {
  if (isNullTree(t))
    return NULL;
  if (strcmp(t->name,x) == 0) {
    return t;
  }
  else if (strcmp(t->name,x) < 0)
    return transfersearchBST(right(t),x);
  else
    return transfersearchBST(left(t),x);
 
}
