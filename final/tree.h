#ifndef _TREE_H_
#define _TREE_H_

typedef struct {
  char name[50];
  char id[6];
  long int elec;
  long int fas;
  long int total;
  int elecnum;
  int fasnum;
} Customer;

typedef Customer element_t;

struct TreeNode {
  element_t data;
  struct TreeNode *left;
  struct TreeNode *right;
};

typedef struct TreeNode* CustomerDB;

CustomerDB createNullTree();
CustomerDB createLeaf(element_t x);
CustomerDB createTree(element_t x, CustomerDB l, CustomerDB r);

int isNullTree(CustomerDB t);
int isLeaf(CustomerDB t);

CustomerDB left(CustomerDB t);
CustomerDB right(CustomerDB t);
element_t value(CustomerDB t);

int sizeOfTree(CustomerDB t);
struct TreeNode* minBST(CustomerDB t);
struct TreeNode* maxBST(CustomerDB t);
CustomerDB insertBST(CustomerDB t, element_t x);
// xoa nut goc va tra ve cay BST sau khi da xoa
CustomerDB removeBST(CustomerDB t,char* x);
struct TreeNode *searchBST(CustomerDB t, char* x);

int height(CustomerDB t);
int leavesCount(CustomerDB t);
int innerNodeCount(CustomerDB t);
int rightChildCount(CustomerDB t);
int nodeCount(CustomerDB t);

CustomerDB reverseBST(CustomerDB t);
long int totalPaymentElec(CustomerDB t);
long int totalPaymentFas(CustomerDB t);
int totalElec(CustomerDB t);
int totalFas(CustomerDB t);

void inorderTravel(CustomerDB t);
struct TreeNode* findMax(CustomerDB t);
CustomerDB readData(CustomerDB t,char* filename);
void freeTree(CustomerDB t);

#endif
