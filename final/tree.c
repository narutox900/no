#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

CustomerDB createNullTree() {
  return (CustomerDB)NULL;
}

CustomerDB createTree(element_t x, CustomerDB l, CustomerDB r) {
  CustomerDB t = (CustomerDB)malloc(sizeof(struct TreeNode));
  t->data = x;
  t->left = l;
  t->right = r;
  return t;
}

CustomerDB createLeaf(element_t x) {
  return createTree(x, createNullTree(), createNullTree());
}

int isNullTree(CustomerDB t) {
  return (t == NULL);
}

int isLeaf(CustomerDB t) {
  return ((t != NULL) &&
          (t->left == NULL) &&
          (t->right == NULL)); 
}

int sizeOfTree(CustomerDB t) {
  if (isNullTree(t))
    return 0;
  else
    return (1 + sizeOfTree(t->left) + sizeOfTree(t->right));
}

CustomerDB left(CustomerDB t) {
  if (isNullTree(t))
    return NULL;
  else return t->left;
}

CustomerDB right(CustomerDB t) {
  if (isNullTree(t))
    return NULL;
  else return t->right;
}

struct TreeNode* minBST(CustomerDB t) {
  struct TreeNode *n = t;
  
  if (isNullTree(t)) return NULL;
  while (!isNullTree(left(n)))
    n = left(n);
  return n;
}

struct TreeNode* maxBST(CustomerDB t) {
  struct TreeNode *n = t;
  
  if (isNullTree(t)) return NULL;
  while (!isNullTree(right(n)))
    n = right(n);
  return n;
}

int height(CustomerDB t) {
  if (isNullTree(t))
    return 0;
  int left = height(t->left);
  int right = height(t->right);
  if (left>=right)
    return left+1;
  else return right+1;
}

int leavesCount(CustomerDB t) {
  int count =0;
  if (isNullTree(t)) return 0;
  if (isLeaf(t)) return 1;
  int left = leavesCount(t->left);
  int right = leavesCount(t->right);
  return left +right;
}
int innerNodeCount(CustomerDB t) {
  if(isNullTree(t)) return 0;
  if (isLeaf(t)) return 0;
  if (!isLeaf(t)) return (innerNodeCount(t->left) + innerNodeCount(t->right) +1);

}
int rightChildCount(CustomerDB t) {
  if (t == NULL)
    return 0;
  int count = 0;
  if (t->right != NULL)
    count = 1;
  return (count + rightChildCount(t->left) + rightChildCount(t->right));
}

int nodeCount(CustomerDB t) {
  if (t == NULL)
    return 0;
  return (1+nodeCount(t->left)+nodeCount(t->right));
}

struct TreeNode *searchBST(CustomerDB t,char*  x) {
  if (isNullTree(t))
    return NULL;
  
  if (strcmp(t->data.id,x) == 0) {
    return t;
  }
  else if (strcmp(t->data.id,x) < 0)
    return searchBST(right(t),x);
  else
    return searchBST(left(t),x);
}

CustomerDB insertBST(CustomerDB t, element_t x) {
  if (isNullTree(t))
    return createLeaf(x);

  if (strcmp(t->data.id,x.id) == 0) {
    t->data.fas += x.fas;
    t->data.elec += x.elec;
    t->data.total += x.total;
    t->data.elecnum += x.elecnum;
    t->data.fasnum += x.fasnum;
    return t;
  }
  else if (strcmp(t->data.id,x.id)>0) {
    t->left = insertBST(t->left, x);
    return t;
  } else {
    t->right = insertBST(t->right, x);
    return t;
  }
}
// xoa nut goc va tra ve cay BST sau khi da xoa
CustomerDB removeRootBST(CustomerDB t) {
  CustomerDB p, tmp;
  
  if (isNullTree(t))
    return NULL;
  // Xoa nut la
  if (isLeaf(t)) {
    free(t);
    return NULL;
  }
  // Xoa nut co 1 con phai
  if (left(t) == NULL) {
    tmp = right(t);
    free(t);
    return tmp;
  }
  // Xoa nut co 1 con trai
  if (right(t) == NULL) {
    tmp = left(t);
    free(t);
    return tmp;
  }
  // Xoa nut co hai con
  p = t;
  tmp = right(t);
  while (left(tmp) != NULL) {
    p = tmp;
    tmp = left(tmp);
  }

  t->data = tmp->data;
  tmp = removeRootBST(tmp);
  if (p == t)
    p->right = tmp;
  else
    p->left = tmp;
  return t;
}

CustomerDB removeBST(CustomerDB t, char* x) {
  CustomerDB p, tmp;
  
  if (isNullTree(t))
    return NULL;

  if (strcmp(t->data.id,x)==0)
    return removeRootBST(t);

  p = NULL;
  tmp = t;
  while ((tmp != NULL) && (strcmp(tmp->data.id,x) !=0)) 
    if (strcmp(tmp->data.id,x)>0) {
      p = tmp;
      tmp = tmp->left;
    } else {
      p = tmp;
      tmp = tmp->right;
    }
  
  if (tmp != NULL) {
    if (p->left == tmp) {
      p->left = removeRootBST(tmp);
    }
    else {
      p->right = removeRootBST(tmp);
    }
  }
  return t;
}

void inorderTravel(CustomerDB t){
  if (t == NULL) {
    //printf("Nil ");
    return;
  }
  if (isLeaf(t)) {
    printf("%-5s",t->data.id);
    printf(" %-30s %-25ld %-20d %-20d \n",t->data.name,t->data.total,t->data.elecnum,t->data.fasnum);
    return;
  }
  inorderTravel(t->left);
  printf("%-5s",t->data.id);
  printf(" %-30s %-25ld %-20d %-20d \n",t->data.name,t->data.total,t->data.elecnum,t->data.fasnum);
  inorderTravel(t->right);
}

struct TreeNode* findMax(CustomerDB t) {
  if (isNullTree(t)) {
    return NULL;
  }
  int i =0;
  long int max = t->data.total;
  struct TreeNode* lmax= findMax(t->left);
  struct TreeNode* rmax= findMax(t->right);
  if (lmax != NULL) {
    if (max < lmax->data.total) {
      max = lmax->data.total;
      i=1;
    }
  }
  if (rmax != NULL) {
    if (max < rmax->data.total) {
      max = rmax->data.total;
      i=2;
    }
  }
  if (i == 0) return t;
  else if (i == 1) return lmax;
  else if (i == 2) return rmax;

}

long int totalPaymentElec(CustomerDB t) {
  if (isNullTree(t))
    return 0;
  return (t->data.elec + totalPaymentElec(t->left) + totalPaymentElec(t->right));
}

long int totalPaymentFas(CustomerDB t) {
  if (isNullTree(t))
    return 0;
  return (t->data.fas + totalPaymentFas(t->left) + totalPaymentFas(t->right));

}

int totalElec(CustomerDB t) {
  if (isNullTree(t))
    return 0;
  return (t->data.elecnum+totalElec(t->left) +totalElec(t->right));
}

int totalFas(CustomerDB t) {
  if (isNullTree(t))
    return 0;
  return (t->data.fasnum+totalFas(t->left) +totalFas(t->right));
}


CustomerDB readData(CustomerDB t,char* filename) {
  FILE *fin = fopen(filename,"r+");
  char name[50];
  char id[6];
  int elec;
  int fas;
  int total;
  char buf[1000];
  int n;
  while (fgets(buf,100,fin)) {
    element_t tmp;
    sscanf(buf," %s %s %ld %ld",tmp.id,tmp.name,&tmp.elec,&tmp.fas);
    if (tmp.elec > 0)
      tmp.elecnum =1;
    else tmp.elecnum=0;
    if (tmp.fas >0)
      tmp.fasnum =1;
    else tmp.fasnum =0;
    tmp.total = tmp.elec + tmp.fas;
    printf("%-5s %-30s %-10ld %-10ld",tmp.id,tmp.name,tmp.elec,tmp.fas);
    t = insertBST(t,tmp);
    struct TreeNode *add = searchBST(t, tmp.id);
    printf("%-10p\n",add);
  }
  return t;
}

void freeTree(CustomerDB t){
  if (t != NULL) {
    freeTree(t->left);
    freeTree(t->right);
    t->left = NULL;
    t->right = NULL;
    free(t);
  }
}
